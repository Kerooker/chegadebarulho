package me.kerooker.chegadebarulho.activity.main.sound.detector.ui

import android.support.constraint.ConstraintSet.PARENT_ID
import android.view.View
import android.widget.FrameLayout
import me.kerooker.chegadebarulho.activity.main.sound.detector.SoundDetectorActivity
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.bottom.SoundDetectorActivityBottomView
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.bottom.soundDetectorActivityBottomView
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.fragment.DetectionFragment
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.fragment.MapFragment
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.recording.SoundDetectorRecordingView
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.recording.soundDetectorRecordingView
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.BOTTOM
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.END
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.START
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.TOP
import org.jetbrains.anko.constraint.layout._ConstraintLayout
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.design.coroutines.onNavigationItemSelected
import org.jetbrains.anko.dip
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.switch

class SoundDetectorActivityUI : AnkoComponent<SoundDetectorActivity> {
    
    private lateinit var bottomView: SoundDetectorActivityBottomView
    private lateinit var container: FrameLayout
    private lateinit var activity: SoundDetectorActivity
    
    
    override fun createView(ui: AnkoContext<SoundDetectorActivity>) = with(ui) {
        activity = this.owner
        constraintLayout {
            createBottomView()
            
            container = frameLayout {
                id = View.generateViewId()
                clipChildren = false
                clipToPadding = false
            }
            
            applyConstraints()
            clipChildren = false
            clipToPadding = false
            switchToDetectionFragment()
        }
    }
    
    private fun _ConstraintLayout.createBottomView() {
        bottomView = soundDetectorActivityBottomView().apply {
            onNavigationItemSelected{
                it!!
                when(it.title) {
                    "Detecção" -> switchToDetectionFragment()
                    "Mapa" -> switchToMapFragment()
                }
            }
        }
    }
    
    private fun switchToDetectionFragment() {
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(container.id, DetectionFragment())
        transaction.commit()
    }
    
    private fun switchToMapFragment() {
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(container.id, MapFragment())
        transaction.commit()
    }
    
    
    private fun _ConstraintLayout.applyConstraints() {
        applyConstraintSet {
            bottomView {
                connect(
                        BOTTOM to BOTTOM of PARENT_ID,
                        START to START of PARENT_ID,
                        END to END of PARENT_ID
                )
            }
            
            container {
                connect(
                        TOP to TOP of PARENT_ID,
                        START to START of PARENT_ID,
                        END to END of PARENT_ID,
                        BOTTOM to TOP of bottomView
                )
                verticalBias = 0f
            }
        }
    }
    
    
}