package me.kerooker.chegadebarulho.activity.main.sound.detector.permission

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import me.kerooker.chegadebarulho.activity.main.sound.detector.SoundDetectorActivity
import kotlin.random.Random
import kotlin.random.Random.Companion

class PermissionRequester(
        private val activity: SoundDetectorActivity
) {
    
    fun requestAudioPermission() {
        if(iHaveAudioPermission()) return
        
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), AUDIO_REQUEST_CODE)
    }
    
    private fun iHaveAudioPermission(): Boolean {
        return ContextCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
    }
    
    companion object {
        val AUDIO_REQUEST_CODE = Random.nextFloat().toShort().toInt()
    }
}