package me.kerooker.chegadebarulho.activity.main.sound.detector.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.android.UI
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.recording.SoundDetectorRecordingView
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.recording.soundDetectorRecordingView
import org.jetbrains.anko.constraint.layout._ConstraintLayout
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.UI

class DetectionFragment : Fragment() {
    
    private lateinit var recordingView: SoundDetectorRecordingView
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            constraintLayout {
                createRecordingView()
                clipChildren = false
                clipToPadding = false
            }
        }.view
    }
    
    private fun _ConstraintLayout.createRecordingView() {
        recordingView = soundDetectorRecordingView()
    }
    
    
}