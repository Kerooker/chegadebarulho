package me.kerooker.chegadebarulho.activity.main.sound.detector.ui.fragment

import android.content.Context.LOCATION_SERVICE
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.location.LocationManager
import android.location.LocationProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import me.kerooker.chegadebarulho.R
import org.jetbrains.anko.defaultSharedPreferences
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.wrapContent

class MapFragment : Fragment() {

    private lateinit var mMapView: MapView
    private lateinit var googleMap: GoogleMap
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UI {
            mMapView = MapView(ctx)
            addView(mMapView, ViewGroup.LayoutParams(wrapContent, wrapContent))
            mMapView.onCreate(savedInstanceState)
            mMapView.onResume()
            
            MapsInitializer.initialize(ctx)
            
            mMapView.getMapAsync {
                googleMap = it
                
                googleMap.isMyLocationEnabled = true
                val my = (ctx.getSystemService(LOCATION_SERVICE) as LocationManager).getLastKnownLocation(LocationManager.GPS_PROVIDER)
                val v = CameraUpdateFactory.newLatLngZoom(LatLng(my.latitude, my.longitude), 17f)
                googleMap.animateCamera(v)
                
                
                
                val voo = ctx.defaultSharedPreferences.all.values.map { str ->
                    str as String
                    val first = str.split(":")[0]
                    val second = str.split(":")[1]
                    LatLng(first.toDouble(), second.toDouble())
                }.groupingBy { it }.eachCount()
                
                voo.forEach { lat, count ->
                    println("$lat $count")
                    val color = when {
                        count < 3 -> 0x16FF0000
                        count < 5 -> 0x33FF0000
                        else -> 0x96FF0000
                    }
                    val circle = CircleOptions().center(lat).radius(100.0).fillColor(color.toInt()).strokeWidth(0f)
                    
                    googleMap.addCircle(circle)
                }
                
            }
            
        }.view
    }
}