package me.kerooker.chegadebarulho.activity.main.sound.detector

import android.Manifest
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import me.kerooker.chegadebarulho.activity.main.sound.detector.analyzer.SoundAnalyzer
import me.kerooker.chegadebarulho.activity.main.sound.detector.permission.PermissionRequester
import me.kerooker.chegadebarulho.activity.main.sound.detector.ui.SoundDetectorActivityUI
import org.jetbrains.anko.setContentView

class SoundDetectorActivity : AppCompatActivity() {
    
    private val permissionRequester = PermissionRequester(this)
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        permissionRequester.requestAudioPermission()
        
        SoundDetectorActivityUI().setContentView(this)
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 10)
    }
    
}

