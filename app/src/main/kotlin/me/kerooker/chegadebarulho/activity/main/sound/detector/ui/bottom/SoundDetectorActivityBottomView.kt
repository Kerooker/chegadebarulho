package me.kerooker.chegadebarulho.activity.main.sound.detector.ui.bottom

import android.content.Context
import android.support.design.widget.BottomNavigationView
import android.view.View
import android.view.ViewManager
import org.jetbrains.anko.custom.ankoView

fun ViewManager.soundDetectorActivityBottomView(): SoundDetectorActivityBottomView {
    return ankoView({ SoundDetectorActivityBottomView(it) }, 0, {})
}

class SoundDetectorActivityBottomView(context: Context) : BottomNavigationView(context) {
    
    init {
        generateId()
        createMenu()
    }
    
    private fun generateId() {
        id = View.generateViewId()
    }
    
    private fun createMenu() {
        menu.apply {
            add("Detecção")
            add("Mapa")
        }
    }
    
    
    
}