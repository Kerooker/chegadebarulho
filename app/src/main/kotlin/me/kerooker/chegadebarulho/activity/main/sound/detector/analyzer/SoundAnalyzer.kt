package me.kerooker.chegadebarulho.activity.main.sound.detector.analyzer

import android.content.Context
import android.media.MediaRecorder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import java.io.File
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.SECONDS
import kotlin.math.roundToInt

class SoundAnalyzer(
        private val context: Context
) {
    
    suspend fun analyze(time: Int = 1, timeUnit: TimeUnit = SECONDS): Decibel {
        val recordedAudio = measureAudioMaxAmplitude(time, timeUnit)
        
        return measureDecibels(recordedAudio)
    }
    
    private suspend fun measureAudioMaxAmplitude(time: Int, timeUnit: TimeUnit): Int {
        return AmplitudeRecorder(context, time, timeUnit).measureAmplitude()
    }
    
    private fun measureDecibels(maxReachedAmplitude: Int): Decibel {
        val transformed = (20 * Math.log10(maxReachedAmplitude.toDouble())).roundToInt()
        
        return Decibel(transformed)
    }
    
}

inline class Decibel(val value: Int)

private class AmplitudeRecorder(
        private val context: Context,
        private val timeToRecord: Int,
        private val timeUnit: TimeUnit
) {

    private var mediaRecorder: MediaRecorder? = null
    private val outputFile: File = createTemporaryFile()
    
    suspend fun measureAmplitude(): Int {
        startRecording()
    
        stopRecordingAfterTimeElapsed()
        
        val amplitude = maxAmplitudeRecorded()
        
        releaseMediaRecorder()
        
        return amplitude
    }
    
    private fun startRecording() {
        configureMediaRecorder()
        mediaRecorder?.start()
        resetMediaRecorderAmplitude()
    }
    
    private fun configureMediaRecorder() {
        mediaRecorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(outputFile.path)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            
            prepare()
        }
    }
    
    private fun resetMediaRecorderAmplitude() {
        mediaRecorder?.maxAmplitude
    }
    
    private suspend fun stopRecordingAfterTimeElapsed() {
        delay(timeToRecord.toLong(), timeUnit)
        stopRecording()
    }
    
    private fun stopRecording() {
        mediaRecorder?.stop()
    }
    
    private fun maxAmplitudeRecorded(): Int {
        return mediaRecorder?.maxAmplitude ?: throw RuntimeException()
        
    }
    
    private fun releaseMediaRecorder() {
        mediaRecorder?.release()
    }
    
    private fun createTemporaryFile(): File {
        return createTempFile(directory = context.cacheDir)
    }
}