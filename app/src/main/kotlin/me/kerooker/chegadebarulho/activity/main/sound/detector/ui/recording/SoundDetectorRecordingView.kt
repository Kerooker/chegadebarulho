package me.kerooker.chegadebarulho.activity.main.sound.detector.ui.recording

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet.PARENT_ID
import android.view.View
import android.view.ViewManager
import android.view.animation.Animation
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import me.kerooker.chegadebarulho.R
import me.kerooker.chegadebarulho.activity.main.sound.detector.analyzer.Decibel
import me.kerooker.chegadebarulho.activity.main.sound.detector.analyzer.SoundAnalyzer
import org.jetbrains.anko.alert
import org.jetbrains.anko.button
import org.jetbrains.anko.cancelButton
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.BOTTOM
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.END
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.START
import org.jetbrains.anko.constraint.layout.ConstraintSetBuilder.Side.TOP
import org.jetbrains.anko.constraint.layout.applyConstraintSet
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.custom.customView
import org.jetbrains.anko.customView
import org.jetbrains.anko.dip
import org.jetbrains.anko.editText
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textView
import org.jetbrains.anko.yesButton
import java.util.concurrent.TimeUnit.MILLISECONDS
import android.location.LocationManager
import org.jetbrains.anko.apply
import org.jetbrains.anko.commit
import org.jetbrains.anko.defaultSharedPreferences
import kotlin.random.Random
import kotlin.random.Random.Companion


fun ViewManager.soundDetectorRecordingView(): SoundDetectorRecordingView {
    return ankoView({ SoundDetectorRecordingView(it) }, 0, {})
}

class SoundDetectorRecordingView(context: Context) : ConstraintLayout(context) {
    
    private lateinit var topText: TextView
    private lateinit var recordingButton: RecordingButton
    private lateinit var reportingOptions: ReportingOptions
    
    init {
        id = View.generateViewId()
        
        createTopText()
        createRecordingButton()
        
        createReportingOptions()
        
        createConstraints()
        clipChildren = false
    }
    
    private fun createTopText() {
        topText = textView("Toque para começar a gravar o barulho") {
            id = View.generateViewId()
            textSize = 18f
        }
    }
    
    private fun createRecordingButton() {
        recordingButton = recordingButton {
            onUpdate = {
                topText.text = "Gravando som... $it"
            }
            
            onRecordingFinish = {
                val isLegal = it < 55
                topText.text = "Som analizado: ${it.toString().format("%.2f")} db\n${if(isLegal)"Está dentro da lei" else "Está acima do permitido!"}"
                mutateButtonForOptions(it)
            }
        }
    }
    
    private fun mutateButtonForOptions(finalValue: Double) {
        if(finalValue < 55) {
            reportingOptions.deactivateReportButton()
        } else {
            reportingOptions.activateReportButton()
        }
        recordingButton.visibility = View.INVISIBLE
        reportingOptions.visibility = View.VISIBLE
    }
    
    private fun createReportingOptions() {
        reportingOptions = reportingOptions {
            visibility = View.INVISIBLE
            
            onCancelClick = {
                this.visibility = View.INVISIBLE
                recordingButton.visibility = View.VISIBLE
            }
            
            onReportClick = {
                context.alert(message = "Informe seu CPF para reportar. Lembre-se: falso testemunho é crime!",
                              title = "Denunciar barulho"
                ) {
                    
                    positiveButton("Confirmar") {
                        reportNoise()
                    }
                    
                    negativeButton("Cancelar") {
                        it.dismiss()
                    }
                    
                    
                    customView {
                        editText {
                            hint = "000.000.000-00"
                        }
                    }
                    
                }.show()
            }
        }
    }
    
    private fun reportNoise() {
        val myLocation = getMyLocation()
        context.defaultSharedPreferences.apply {
            putString(Random.nextInt().toString(), "${myLocation.latitude}:${myLocation.longitude}")
        }
    }
    
    private fun getMyLocation(): LatLng {
        val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val longitude = location.longitude
        val latitude = location.latitude
        return LatLng(latitude, longitude)
    }
    
    private fun createConstraints() {
        applyConstraintSet {
            topText {
                connect(
                    TOP to TOP of PARENT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }
            
            recordingButton {
                connect(
                        START to START of PARENT_ID,
                        END to END of PARENT_ID,
                        TOP to BOTTOM of topText margin dip(90),
                        BOTTOM to BOTTOM of PARENT_ID
                )
            }
            
            reportingOptions {
                connect(
                    START to START of recordingButton,
                    END to END of recordingButton,
                    TOP to TOP of recordingButton,
                    BOTTOM to BOTTOM of recordingButton
                )
            }
        }
    }
    
}

private fun ViewManager.recordingButton(init: RecordingButton.() -> Unit): RecordingButton {
    return ankoView({ RecordingButton(it) }, 0, init)
}

private class RecordingButton(context: Context) : ImageView(context) {
    
    private var isRecording = false
    private lateinit var sizeAnimator: ObjectAnimator
    
    var onRecordingFinish: (averageSound: Double) -> Unit = {  }
    
    var onUpdate: (remainingSeconds: Int) -> Unit = {  }
    
    init {
        id = View.generateViewId()
        
        setButtonImage()
        createAnimationOnClick()
        
        setSize()
    }
    
    private fun setButtonImage() {
        imageResource = R.drawable.logo_som
    }
    
    private fun createAnimationOnClick() {
        onClick {
            if(isRecording) return@onClick
            recording {
                startAnimations()
                val analyzed = recordAndAnalyzeTenSeconds()
                onRecordingFinish(analyzed)
            }
            
            resetState()
        }
    }
    
    private inline fun recording(block: () -> Unit) {
        isRecording = true
        block()
        isRecording = false
    }
    
    private fun startAnimations() {
        startRotationAnimation()
        startSizePulseAnimation()
    }
    
    private fun startRotationAnimation() {
        val anim = createRotateAnimation()
        startAnimation(anim)
    }
    
    private fun createRotateAnimation(): RotateAnimation {
        return RotateAnimation(0f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f).apply {
            interpolator = LinearInterpolator()
            duration = ROTATION_DURATION_MILLIS
            repeatCount = Animation.INFINITE
        }
    }
    
    private fun startSizePulseAnimation() {
        sizeAnimator = createSizePulseAnimation()
        sizeAnimator.start()
    }
    
    private fun createSizePulseAnimation(): ObjectAnimator {
        return ObjectAnimator.ofPropertyValuesHolder(
                this,
                PropertyValuesHolder.ofFloat("scaleX", SIZE_PULSE_TRANSFORM_PERCENTAGE),
                PropertyValuesHolder.ofFloat("scaleY", SIZE_PULSE_TRANSFORM_PERCENTAGE)
        ).apply {
            duration = SIZE_PULSE_DURATION_MILLIS
            repeatCount = ValueAnimator.INFINITE
            repeatMode = ValueAnimator.REVERSE// Keep increasing and decreasing
        }
    }
    
    private suspend fun recordAndAnalyzeTenSeconds(): Double {
        val pulses = async { collectTenSeconds() }.await()
        return pulses.dropExtremes().averageBiggestFive()
    }
    
    private suspend fun collectTenSeconds(): List<Decibel> {
        return List(20) {
            launch(Dispatchers.Main) {
                onUpdate((20 - it) / 2)
            }
            collectHalfSecondPulse()
        }
    }
    
    private suspend fun collectHalfSecondPulse(): Decibel {
        return SoundAnalyzer(context).analyze(500, MILLISECONDS)
    }
    
    private fun List<Decibel>.dropExtremes(): List<Decibel> {
        return this.sortedBy { it.value }.drop(1).dropLast(1)
    }
    
    private fun List<Decibel>.averageBiggestFive(): Double {
        return this.takeLast(5).map { it.value }.average()
    }
    
    private fun resetState() {
        resetSize()
        resetAnimations()
    }
    
    private fun resetSize() {
        scaleX = 1f
        scaleY = 1f
    }
    
    private fun resetAnimations() {
        clearAnimation()
        sizeAnimator.cancel()
    }
    
    private fun setSize() {
        layoutParams = ConstraintLayout.LayoutParams(dip(160), dip(160))
    }
    
    companion object {
        private const val ROTATION_DURATION_MILLIS = 2500L
        private const val SIZE_PULSE_DURATION_MILLIS = 200L
        private const val SIZE_PULSE_TRANSFORM_PERCENTAGE = 1.25f
    }
    
}

private fun ViewManager.reportingOptions(init: ReportingOptions.() -> Unit = { } ): ReportingOptions {
    return ankoView({ ReportingOptions(it) }, 0, init)
}

private class ReportingOptions(context: Context) : ConstraintLayout(context) {
    
    private lateinit var reportButton: Button
    private lateinit var cancelButton: Button
    
    var onReportClick = { }
    var onCancelClick = { }
    
    init {
        id = View.generateViewId()
        
        createReportButton()
        createCancelButton()
        
        setConstraints()
    }
    
    private fun createReportButton() {
        reportButton = button("Reportar barulho") {
            id = View.generateViewId()
            onClick { onReportClick() }
        }
    }
    
    fun activateReportButton() {
        reportButton.isEnabled = true
    }
    
    fun deactivateReportButton() {
        reportButton.isEnabled = false
    }
    
    private fun createCancelButton() {
        cancelButton = button("Gravar novamente") {
            id = View.generateViewId()
            onClick { onCancelClick() }
        }
    }
    
    private fun setConstraints() {
        applyConstraintSet {
            reportButton {
                connect(
                    TOP to TOP of PARENT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }
            
            cancelButton {
                connect(
                    TOP to BOTTOM of reportButton margin dip(8),
                    BOTTOM to BOTTOM of PARENT_ID,
                    START to START of PARENT_ID,
                    END to END of PARENT_ID
                )
            }
        }
    }
}